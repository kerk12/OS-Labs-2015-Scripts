fileList=$(ls)
for file in $fileList
do
	if [ -f "$file" ]
	then
		if [ -w "$file" ]
		then
			mv "$file" "$file.del"
		fi
	fi
done
