fileList=$(ls)

fileNameFinal=null
wordCount=0
for file in $fileList
do
	if [ -f "$file" ]
	then
		wordCountOnFile=$(grep "$1" "./$file" | wc -l)
		if [ "$wordCountOnFile" -gt "$wordCount" ]
		then
			wordCount=$wordCountOnFile
			fileNameFinal="$file"
		elif [ "$wordCountOnFile" -eq "$wordCount" ]
		then
			fileNameFinal="$fileNameFinal $file"
		fi
	fi
done

if [ "$wordCount" -eq 0 ]
then
	echo "The word was not found on any of the files."
	exit 0
fi
echo $(cat $fileNameFinal)
